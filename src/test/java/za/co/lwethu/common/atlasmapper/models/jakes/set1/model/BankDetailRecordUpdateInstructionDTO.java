package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BankDetailRecordUpdateInstructionDTO implements Serializable {
    private String workflowProcessId;
    private ProductPaymentAdviceRecordDTO currentBankAccountRecord;
    private ProductBankAccountPaymentAdviceRecordDTO newBankAccountRecord;
    private List<SupportingDocumentDTO> supportingDocuments;

    public String getWorkflowProcessId() {
        return workflowProcessId;
    }

    public void setWorkflowProcessId(String workflowProcessId) {
        this.workflowProcessId = workflowProcessId;
    }

    public ProductPaymentAdviceRecordDTO getCurrentBankAccountRecord() {
        return currentBankAccountRecord;
    }

    public void setCurrentBankAccountRecord(ProductPaymentAdviceRecordDTO currentBankAccountRecord) {
        this.currentBankAccountRecord = currentBankAccountRecord;
    }

    public ProductBankAccountPaymentAdviceRecordDTO getNewBankAccountRecord() {
        return newBankAccountRecord;
    }

    public void setNewBankAccountRecord(ProductBankAccountPaymentAdviceRecordDTO newBankAccountRecord) {
        this.newBankAccountRecord = newBankAccountRecord;
    }

    public List<SupportingDocumentDTO> getSupportingDocuments() {
        if(supportingDocuments == null){
            supportingDocuments = new ArrayList<>();
        }
        return supportingDocuments;
    }
}
