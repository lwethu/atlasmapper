package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class EntityDetailsDTO extends BankAccountOwnerDTO implements Serializable {
    private long entityNumber;
    private String nonPersonFullName;
    private EntityFirstNameSurnameDTO entityNameSurname;
    private String legalReferenceNumber;
    private String legalReferenceNumberType;
    private String nationality;
    private String entityRole;

    public long getEntityNumber() {
        return entityNumber;
    }

    public void setEntityNumber(long entityNumber) {
        this.entityNumber = entityNumber;
    }

    public String getNonPersonFullName() {
        return nonPersonFullName;
    }

    public void setNonPersonFullName(String nonPersonFullName) {
        this.nonPersonFullName = nonPersonFullName;
    }

    public EntityFirstNameSurnameDTO getEntityNameSurname() {
        return entityNameSurname;
    }

    public void setEntityNameSurname(EntityFirstNameSurnameDTO entityNameSurname) {
        this.entityNameSurname = entityNameSurname;
    }

    public String getLegalReferenceNumber() {
        return legalReferenceNumber;
    }

    public void setLegalReferenceNumber(String legalReferenceNumber) {
        this.legalReferenceNumber = legalReferenceNumber;
    }

    public String getLegalReferenceNumberType() {
        return legalReferenceNumberType;
    }

    public void setLegalReferenceNumberType(String legalReferenceNumberType) {
        this.legalReferenceNumberType = legalReferenceNumberType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEntityRole() {
        return entityRole;
    }

    public void setEntityRole(String entityRole) {
        this.entityRole = entityRole;
    }
}
