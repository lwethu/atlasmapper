package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.List;

public class UpdateBankDetailsInstructionPerProductDTO implements Serializable {
    private List<BankDetailRecordUpdateInstructionDTO> bankDetailRecordUpdateInstruction;
}
