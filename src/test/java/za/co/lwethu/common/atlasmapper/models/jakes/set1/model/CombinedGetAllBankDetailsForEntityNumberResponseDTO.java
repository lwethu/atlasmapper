package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
public class CombinedGetAllBankDetailsForEntityNumberResponseDTO
        extends CombinedGetAllBankDetailsForEntityNumberResponseMessageDTO implements Serializable {
    private Float schemaVersion;

    public Float getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(Float schemaVersion) {
        this.schemaVersion = schemaVersion;
    }
}
