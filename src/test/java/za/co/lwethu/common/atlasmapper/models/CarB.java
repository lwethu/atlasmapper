package za.co.lwethu.common.atlasmapper.models;

public class CarB extends Vehicle {

    private Integer id;
    private ColorEnum color;
    private Double price;
    private PersonB person;
    private Boolean preOwned;

    public CarB() {
    }

    public CarB(Integer id, ColorEnum color, Double price, PersonB person, Boolean preOwned, Integer mileage,byte[] image) {
        this.id = id;
        this.color = color;
        this.price = price;
        this.person = person;
        this.preOwned = preOwned;
        setMileage(mileage);
        setImage(image);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ColorEnum getColor() {
        return color;
    }

    public void setColor(ColorEnum color) {
        this.color = color;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PersonB getPerson() {
        return person;
    }

    public void setPerson(PersonB person) {
        this.person = person;
    }

    public Boolean getPreOwned() {
        return preOwned;
    }

    public void setPreOwned(Boolean preOwned) {
        this.preOwned = preOwned;
    }
}
