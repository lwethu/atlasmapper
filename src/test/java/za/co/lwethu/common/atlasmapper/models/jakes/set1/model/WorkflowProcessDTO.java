package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WorkflowProcessDTO implements Serializable {
    private Long processId;
    private String processStatus;
    private List<WorkflowProcessDTO> workflowProcess;

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public List<WorkflowProcessDTO> getWorkflowProcess() {
        if(workflowProcess == null){
            workflowProcess = new ArrayList<>();
        }
        return workflowProcess;
    }
}
