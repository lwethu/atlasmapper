package za.co.lwethu.common.atlasmapper.services;

public class MapAge extends MapToService {

    @Override
    public Object map(Object value) {
        Integer input = (Integer)value;
        return String.valueOf(input);
    }

}
