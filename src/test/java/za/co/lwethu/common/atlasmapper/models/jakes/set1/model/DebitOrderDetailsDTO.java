package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

//import za.co.discovery.gss.cas.cadi.bankdetail.enums.DebitOrderFrequencyEnum;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class DebitOrderDetailsDTO implements Serializable {
    private BigInteger debitOrderDayOfMonth;
    //private DebitOrderFrequencyEnum debitOrderFrequency;
    private BigDecimal lastDebitOrderAmount;

    public BigInteger getDebitOrderDayOfMonth() {
        return debitOrderDayOfMonth;
    }

    public void setDebitOrderDayOfMonth(BigInteger debitOrderDayOfMonth) {
        this.debitOrderDayOfMonth = debitOrderDayOfMonth;
    }

    /*public DebitOrderFrequencyEnum getDebitOrderFrequency() {
        return debitOrderFrequency;
    }

    public void setDebitOrderFrequency(DebitOrderFrequencyEnum debitOrderFrequency) {
        this.debitOrderFrequency = debitOrderFrequency;
    }*/

    public BigDecimal getLastDebitOrderAmount() {
        return lastDebitOrderAmount;
    }

    public void setLastDebitOrderAmount(BigDecimal lastDebitOrderAmount) {
        this.lastDebitOrderAmount = lastDebitOrderAmount;
    }
}
