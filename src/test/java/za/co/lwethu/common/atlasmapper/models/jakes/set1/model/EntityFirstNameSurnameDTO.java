package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class EntityFirstNameSurnameDTO implements Serializable {
    private String firstName;
    private String surname;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
