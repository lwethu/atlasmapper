package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class NotificationEngineDocumentDTO  implements Serializable {
    private String repositoryUrl;
    private int processNumber;
    private String documentReference;
}
