package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductHouseDetailDTO implements Serializable {
    private String code;
    private String name;
    private List<String> contactNumber;
    private List<String> contactEmail;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getContactNumber() {
        if (contactNumber == null){
            contactNumber = new ArrayList<>();
        }
        return contactNumber;
    }

    public List<String> getContactEmail() {
        if (contactEmail == null){
            contactEmail = new ArrayList<>();
        }
        return contactEmail;
    }
}
