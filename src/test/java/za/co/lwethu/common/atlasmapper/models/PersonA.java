package za.co.lwethu.common.atlasmapper.models;

import java.util.List;
import java.util.Set;

public class PersonA {

    private Integer id;
    private String name;
    //@MapTo(MapAge.class)
    private Integer age;
    private Set<String> legalRefs;
    private List<CarA> cars;

    public PersonA() {
    }

    public PersonA(Integer id, String name, Integer age, Set<String> legalRefs, List<CarA> cars) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.cars = cars;
        this.legalRefs = legalRefs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Set<String> getLegalRefs() {
        return legalRefs;
    }

    public void setLegalRefs(Set<String> legalRefs) {
        this.legalRefs = legalRefs;
    }

    public List<CarA> getCars() {
        return cars;
    }

    public void setCars(List<CarA> cars) {
        this.cars = cars;
    }

}
