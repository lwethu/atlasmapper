package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class ProductBankAccountPaymentAdviceRecordDTO implements Serializable {
    private BankAccountOwnerDTO bankAccountOwner;
    private BankDetailDTO bankAccountDetail;
    private DebitOrderDetailsDTO debitOrderDetails;

    public BankAccountOwnerDTO getBankAccountOwner() {
        return bankAccountOwner;
    }

    public void setBankAccountOwner(BankAccountOwnerDTO bankAccountOwner) {
        this.bankAccountOwner = bankAccountOwner;
    }

    public BankDetailDTO getBankAccountDetail() {
        return bankAccountDetail;
    }

    public void setBankAccountDetail(BankDetailDTO bankAccountDetail) {
        this.bankAccountDetail = bankAccountDetail;
    }

    public DebitOrderDetailsDTO getDebitOrderDetails() {
        return debitOrderDetails;
    }

    public void setDebitOrderDetails(DebitOrderDetailsDTO debitOrderDetails) {
        this.debitOrderDetails = debitOrderDetails;
    }

}
