package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import org.springframework.stereotype.Component;

@Component
public class CombinedGetAllBankDetailsForEntityNumberRequestDTO {
    private Float schemaVersion;
    private GetAllBankDetailsForEntityNumberRequestDTO getAllBankDetailsForEntityNumberRequest;
    private RequesterDetailsDTO requesterDetailsType;

    public Float getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(Float schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public GetAllBankDetailsForEntityNumberRequestDTO getGetAllBankDetailsForEntityNumberRequest() {
        return getAllBankDetailsForEntityNumberRequest;
    }

    public void setGetAllBankDetailsForEntityNumberRequest(GetAllBankDetailsForEntityNumberRequestDTO getAllBankDetailsForEntityNumberRequest) {
        this.getAllBankDetailsForEntityNumberRequest = getAllBankDetailsForEntityNumberRequest;
    }

    public RequesterDetailsDTO getRequesterDetailsType() {
        return requesterDetailsType;
    }

    public void setRequesterDetailsType(RequesterDetailsDTO requesterDetailsType) {
        this.requesterDetailsType = requesterDetailsType;
    }
}
