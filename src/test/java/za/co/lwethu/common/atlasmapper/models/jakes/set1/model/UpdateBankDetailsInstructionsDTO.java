package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.List;

public class UpdateBankDetailsInstructionsDTO implements Serializable {
    private List<UpdateBankDetailsInstructionPerProduct> updateBankDetailsInstructionPerProduct;

    private class UpdateBankDetailsInstructionPerProduct extends UpdateBankDetailsInstructionPerProductDTO{
    }
}
