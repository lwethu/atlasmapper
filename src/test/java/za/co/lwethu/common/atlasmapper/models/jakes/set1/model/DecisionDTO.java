package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class DecisionDTO implements Serializable {
    private boolean changeAllowed;
    private String message;

    public boolean isChangeAllowed() {
        return changeAllowed;
    }

    public void setChangeAllowed(boolean changeAllowed) {
        this.changeAllowed = changeAllowed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
