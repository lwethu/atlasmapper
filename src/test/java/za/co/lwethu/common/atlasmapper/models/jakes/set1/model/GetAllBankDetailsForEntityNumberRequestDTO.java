package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;


import org.springframework.stereotype.Component;

import java.io.Serializable;
@Component
public class GetAllBankDetailsForEntityNumberRequestDTO implements Serializable {
    private Long entityNumber;
    private String preloadMemberToken;
    private String groupGUID;

    public Long getEntityNumber() {
        return entityNumber;
    }

    public void setEntityNumber(Long entityNumber) {
        this.entityNumber = entityNumber;
    }

    public String getPreloadMemberToken() {
        return preloadMemberToken;
    }

    public void setPreloadMemberToken(String preloadMemberToken) {
        this.preloadMemberToken = preloadMemberToken;
    }

    public String getGroupGUID() {
        return groupGUID;
    }

    public void setGroupGUID(String groupGUID) {
        this.groupGUID = groupGUID;
    }
}
