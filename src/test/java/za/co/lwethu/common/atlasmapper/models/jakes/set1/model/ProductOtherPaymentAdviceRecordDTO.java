package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class ProductOtherPaymentAdviceRecordDTO implements Serializable  {
    private Boolean payByEft;
    private Boolean payByCash;
    private Boolean payByCheque;
    private Boolean employerPaid;
    private Boolean initiatingProductHousePaid;

    public Boolean getPayByEft() {
        return payByEft;
    }

    public void setPayByEft(Boolean payByEft) {
        this.payByEft = payByEft;
    }

    public Boolean getPayByCash() {
        return payByCash;
    }

    public void setPayByCash(Boolean payByCash) {
        this.payByCash = payByCash;
    }

    public Boolean getPayByCheque() {
        return payByCheque;
    }

    public void setPayByCheque(Boolean payByCheque) {
        this.payByCheque = payByCheque;
    }

    public Boolean getEmployerPaid() {
        return employerPaid;
    }

    public void setEmployerPaid(Boolean employerPaid) {
        this.employerPaid = employerPaid;
    }

    public Boolean getInitiatingProductHousePaid() {
        return initiatingProductHousePaid;
    }

    public void setInitiatingProductHousePaid(Boolean initiatingProductHousePaid) {
        this.initiatingProductHousePaid = initiatingProductHousePaid;
    }
}
