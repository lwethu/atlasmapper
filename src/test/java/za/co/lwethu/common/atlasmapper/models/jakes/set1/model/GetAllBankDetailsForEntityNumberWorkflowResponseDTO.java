package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetAllBankDetailsForEntityNumberWorkflowResponseDTO implements Serializable {
    private List<WorkflowProcessDTO> workflowProcess;

    public List<WorkflowProcessDTO> getWorkflowProcess() {
        if(workflowProcess == null){
            workflowProcess = new ArrayList<>();
        }
        return workflowProcess;
    }
}
