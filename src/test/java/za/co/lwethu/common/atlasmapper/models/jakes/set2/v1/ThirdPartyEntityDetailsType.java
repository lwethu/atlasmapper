//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.08.10 at 08:54:22 AM CAT 
//


package za.co.lwethu.common.atlasmapper.models.jakes.set2.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Populate this element if not all details, if any is known, of the Entity that owns the
 *                 Bank account is known
 *             
 * 
 * <p>Java class for ThirdPartyEntityDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ThirdPartyEntityDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.discovery.co.za/cadi}BankAccountOwnerType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entityNumber" type="{http://schemas.discovery.co.za/cadi}EntityNumberType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="nonPersonFullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *           &lt;element name="entityNameSurname" type="{http://schemas.discovery.co.za/cadi}EntityFirstNameSurnameType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="legalReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="legalReferenceNumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entityRole" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="sex" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ThirdPartyEntityDetailsType", propOrder = {
    "entityNumber",
    "nonPersonFullName",
    "entityNameSurname",
    "legalReferenceNumber",
    "legalReferenceNumberType",
    "nationality",
    "entityRole",
    "dateOfBirth",
    "sex"
})
public class ThirdPartyEntityDetailsType
    extends BankAccountOwnerType
{

    protected Long entityNumber;
    protected String nonPersonFullName;
    protected EntityFirstNameSurnameType entityNameSurname;
    protected String legalReferenceNumber;
    protected String legalReferenceNumberType;
    protected String nationality;
    protected String entityRole;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(required = true)
    protected String sex;

    /**
     * Gets the value of the entityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEntityNumber() {
        return entityNumber;
    }

    /**
     * Sets the value of the entityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEntityNumber(Long value) {
        this.entityNumber = value;
    }

    /**
     * Gets the value of the nonPersonFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonPersonFullName() {
        return nonPersonFullName;
    }

    /**
     * Sets the value of the nonPersonFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonPersonFullName(String value) {
        this.nonPersonFullName = value;
    }

    /**
     * Gets the value of the entityNameSurname property.
     * 
     * @return
     *     possible object is
     *     {@link EntityFirstNameSurnameType }
     *     
     */
    public EntityFirstNameSurnameType getEntityNameSurname() {
        return entityNameSurname;
    }

    /**
     * Sets the value of the entityNameSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityFirstNameSurnameType }
     *     
     */
    public void setEntityNameSurname(EntityFirstNameSurnameType value) {
        this.entityNameSurname = value;
    }

    /**
     * Gets the value of the legalReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalReferenceNumber() {
        return legalReferenceNumber;
    }

    /**
     * Sets the value of the legalReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalReferenceNumber(String value) {
        this.legalReferenceNumber = value;
    }

    /**
     * Gets the value of the legalReferenceNumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalReferenceNumberType() {
        return legalReferenceNumberType;
    }

    /**
     * Sets the value of the legalReferenceNumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalReferenceNumberType(String value) {
        this.legalReferenceNumberType = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the entityRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityRole() {
        return entityRole;
    }

    /**
     * Sets the value of the entityRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityRole(String value) {
        this.entityRole = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

}
