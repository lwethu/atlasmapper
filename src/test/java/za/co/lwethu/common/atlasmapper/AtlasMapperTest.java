package za.co.lwethu.common.atlasmapper;

import org.dozer.DozerBeanMapper;
import org.junit.Test;
import za.co.lwethu.common.atlasmapper.enums.AtlasMapperParamTypeEnum;
import za.co.lwethu.common.atlasmapper.models.*;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AtlasMapperTest {

    private AtlasMapper atlasMapper = new AtlasMapper();
    private DozerBeanMapper dozerMapper = new DozerBeanMapper();

    @Test
    public void mapNormalObject() throws Exception {
        PersonA personA = new PersonA(1, "Lwazi", 29, new HashSet<String>(), new ArrayList<CarA>());
        CarA carA = new CarA(1, ColorEnum.BLUE, 12.32, personA, true, 40000, new byte[]{1, 2, 3});
        CarB carB = atlasMapper.map(carA, CarB.class);
        assertTrue(isCarEqual(carA, carB));
    }

    private Boolean isCarEqual(CarA carA, CarB carB) {
        return Objects.equals(carA.getId(), carB.getId())
                && Objects.equals(carA.getColor(), carB.getColor())
                && Objects.equals(carA.getPrice(), carB.getPrice())
                && Objects.equals(carA.getId(), carB.getId())
                && isPersonEqual(carA.getPerson(), carB.getPerson());
    }

    private Boolean isPersonEqual(PersonA personA, PersonB personB) {
        return Objects.equals(personA.getId(), personB.getId())
                && Objects.equals(personA.getAge(), Integer.valueOf(personB.getAge()))
                && Objects.equals(personA.getName(), personB.getName())
                && Objects.equals(personA.getLegalRefs(), personB.getLegalRefs())
                && Objects.equals(personA.getCars(), personB.getCars());
    }

    @Test
    public void mapDozerNormalObject() throws Exception {
        PersonA personA = new PersonA(1, "Lwazi", 29, new HashSet<String>(), new ArrayList<CarA>());
        CarA carA = new CarA(1, ColorEnum.BLUE, 12.32, personA, true, 40000, new byte[]{1, 2, 3});
        CarB carB = dozerMapper.map(carA, CarB.class);
        assertTrue(isCarEqual(carA, carB));
    }

    @Test
    public void mapNormalObjectWithParams() throws Exception {
        PersonA personA = new PersonA(1, "Lwazi", 29, new HashSet<String>(), new ArrayList<CarA>());
        CarA carA = new CarA(1, ColorEnum.BLUE, 12.32, personA, true, 40000, new byte[]{1, 2, 3});
        CarB carB = atlasMapper.map(carA, CarB.class, "id", "color", "person.id");
        assertEquals(carA.getId(), carB.getId());
        assertEquals(carA.getColor(), carB.getColor());
        assertEquals(carA.getPerson().getId(), carB.getPerson().getId());
    }

    @Test
    public void mapNormalObjectWithListChild() throws Exception {
        CarA carA = new CarA(1, ColorEnum.BLUE, 12.32, null, true, 40000, new byte[]{1, 2, 3});
        PersonA personA = new PersonA(1, "Lwazi", 29, new HashSet<String>(), Arrays.asList((carA)));
        PersonB personB = atlasMapper.map(personA, PersonB.class);
        assertTrue(isPersonEqual(personA, personB));
    }

    @Test
    public void mapNormalObjectWithListChildButBasicType() throws Exception {
        CarA carA = new CarA(1, ColorEnum.BLUE, 12.32, null, true, 40000, new byte[]{1, 2, 3});
        PersonA personA = new PersonA(1, "Lwazi", 29, new HashSet<String>(), Arrays.asList((carA)));
        PersonB personB = atlasMapper.map(personA, PersonB.class, AtlasMapperParamTypeEnum.BASIC);
        personA.setCars(null);
        personA.setLegalRefs(null);
        assertTrue(isPersonEqual(personA, personB));
    }

    @Test
    public void mapDozerNNormalObjectWithListChild() throws Exception {
        CarA carA = new CarA(1, ColorEnum.BLUE, 12.32, null, true, 40000, new byte[]{1, 2, 3});
        PersonA personA = new PersonA(1, "Lwazi", 29, new HashSet<String>(), Arrays.asList((carA)));
        PersonB personB = dozerMapper.map(personA, PersonB.class);
        assertTrue(isPersonEqual(personA, personB));
    }

    @Test
    public void mapNormalListObject() throws Exception {
        PersonA personA = new PersonA(1, "Lwazi", 29, new HashSet<String>(), new ArrayList<CarA>());
        List<CarA> carsA = Arrays.asList(
                new CarA(1, ColorEnum.BLUE, 12.32, personA, true, 40000, new byte[]{1, 2, 3}),
                new CarA(2, ColorEnum.BLACK, 22.32, personA, true, 40000, new byte[]{1, 2, 3}),
                new CarA(3, ColorEnum.GREEN, 2.10, personA, true, 40000, new byte[]{1, 2, 3}));
        List<CarB> carsB = atlasMapper.map(carsA, CarB.class);
        assertEquals(1, (int) carsB.get(0).getId());
        assertEquals(2, (int) carsB.get(1).getId());
        assertEquals(3, (int) carsB.get(2).getId());
    }

}
