//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.08.10 at 08:54:22 AM CAT 
//


package za.co.lwethu.common.atlasmapper.models.jakes.set2.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *                 The workflow process details.
 *                 This type contains the workflow processes as well as detail required by the client
 *             
 * 
 * <p>Java class for WorkflowProcessType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkflowProcessType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="processId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="processStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nextActionCode" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="nextAction" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="lastActionCode" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="lastAction" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="limboActionCode" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="limboAction" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nextDueLocalDateTime" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="workflowProcess" type="{http://schemas.discovery.co.za/cadi}WorkflowProcessType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkflowProcessType", propOrder = {
    "processId",
    "processStatus",
    "nextActionCode",
    "nextAction",
    "lastActionCode",
    "lastAction",
    "limboActionCode",
    "limboAction",
    "nextDueLocalDateTime",
    "workflowProcess"
})
public class WorkflowProcessType {

    protected long processId;
    @XmlElement(required = true)
    protected String processStatus;
    protected long nextActionCode;
    @XmlElement(required = true)
    protected String nextAction;
    protected long lastActionCode;
    @XmlElement(required = true)
    protected String lastAction;
    protected long limboActionCode;
    @XmlElement(required = true)
    protected String limboAction;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nextDueLocalDateTime;
    protected List<WorkflowProcessType> workflowProcess;

    /**
     * Gets the value of the processId property.
     * 
     */
    public long getProcessId() {
        return processId;
    }

    /**
     * Sets the value of the processId property.
     * 
     */
    public void setProcessId(long value) {
        this.processId = value;
    }

    /**
     * Gets the value of the processStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessStatus() {
        return processStatus;
    }

    /**
     * Sets the value of the processStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessStatus(String value) {
        this.processStatus = value;
    }

    /**
     * Gets the value of the nextActionCode property.
     * 
     */
    public long getNextActionCode() {
        return nextActionCode;
    }

    /**
     * Sets the value of the nextActionCode property.
     * 
     */
    public void setNextActionCode(long value) {
        this.nextActionCode = value;
    }

    /**
     * Gets the value of the nextAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextAction() {
        return nextAction;
    }

    /**
     * Sets the value of the nextAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextAction(String value) {
        this.nextAction = value;
    }

    /**
     * Gets the value of the lastActionCode property.
     * 
     */
    public long getLastActionCode() {
        return lastActionCode;
    }

    /**
     * Sets the value of the lastActionCode property.
     * 
     */
    public void setLastActionCode(long value) {
        this.lastActionCode = value;
    }

    /**
     * Gets the value of the lastAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastAction() {
        return lastAction;
    }

    /**
     * Sets the value of the lastAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastAction(String value) {
        this.lastAction = value;
    }

    /**
     * Gets the value of the limboActionCode property.
     * 
     */
    public long getLimboActionCode() {
        return limboActionCode;
    }

    /**
     * Sets the value of the limboActionCode property.
     * 
     */
    public void setLimboActionCode(long value) {
        this.limboActionCode = value;
    }

    /**
     * Gets the value of the limboAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimboAction() {
        return limboAction;
    }

    /**
     * Sets the value of the limboAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimboAction(String value) {
        this.limboAction = value;
    }

    /**
     * Gets the value of the nextDueLocalDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextDueLocalDateTime() {
        return nextDueLocalDateTime;
    }

    /**
     * Sets the value of the nextDueLocalDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextDueLocalDateTime(XMLGregorianCalendar value) {
        this.nextDueLocalDateTime = value;
    }

    /**
     * Gets the value of the workflowProcess property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the workflowProcess property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWorkflowProcess().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WorkflowProcessType }
     * 
     * 
     */
    public List<WorkflowProcessType> getWorkflowProcess() {
        if (workflowProcess == null) {
            workflowProcess = new ArrayList<WorkflowProcessType>();
        }
        return this.workflowProcess;
    }

}
