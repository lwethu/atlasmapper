package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import org.springframework.stereotype.Component;

import java.io.Serializable;
@Component
public class GetAllBankDetailsForEntityNumberResponseDTO implements Serializable {
    private String groupGUID;
    private ProductHouseDetailDTO productHouseDetail;
    private ProductCollectionDTO productCollection;
    private InstructionResponseDTO getInstructionResponse;

    public String getGroupGUID() {
        return groupGUID;
    }

    public void setGroupGUID(String groupGUID) {
        this.groupGUID = groupGUID;
    }

    public ProductHouseDetailDTO getProductHouseDetail() {
        return productHouseDetail;
    }

    public void setProductHouseDetail(ProductHouseDetailDTO productHouseDetail) {
        this.productHouseDetail = productHouseDetail;
    }

    public ProductCollectionDTO getProductCollection() {
        return productCollection;
    }

    public void setProductCollection(ProductCollectionDTO productCollection) {
        this.productCollection = productCollection;
    }

    public InstructionResponseDTO getGetInstructionResponse() {
        return getInstructionResponse;
    }

    public void setGetInstructionResponse(InstructionResponseDTO getInstructionResponse) {
        this.getInstructionResponse = getInstructionResponse;
    }
}
