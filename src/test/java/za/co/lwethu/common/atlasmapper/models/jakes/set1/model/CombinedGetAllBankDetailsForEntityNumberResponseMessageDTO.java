package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.util.ArrayList;
import java.util.List;

public class CombinedGetAllBankDetailsForEntityNumberResponseMessageDTO {
    private List<GetAllBankDetailsForEntityNumberResponseDTO> getAllBankDetailsForEntityNumberResponse;
    private GetAllBankDetailsForEntityNumberWorkflowResponseDTO getAllBankDetailsForEntityNumberWorkflowResponse;


    public List<GetAllBankDetailsForEntityNumberResponseDTO> getGetAllBankDetailsForEntityNumberResponse() {
        if (getAllBankDetailsForEntityNumberResponse == null){
            getAllBankDetailsForEntityNumberResponse = new ArrayList<>();
        }
        return getAllBankDetailsForEntityNumberResponse;
    }

    public GetAllBankDetailsForEntityNumberWorkflowResponseDTO getGetAllBankDetailsForEntityNumberWorkflowResponse() {
        return getAllBankDetailsForEntityNumberWorkflowResponse;
    }

    public void setGetAllBankDetailsForEntityNumberWorkflowResponse(GetAllBankDetailsForEntityNumberWorkflowResponseDTO getAllBankDetailsForEntityNumberWorkflowResponse) {
        this.getAllBankDetailsForEntityNumberWorkflowResponse = getAllBankDetailsForEntityNumberWorkflowResponse;
    }
}
