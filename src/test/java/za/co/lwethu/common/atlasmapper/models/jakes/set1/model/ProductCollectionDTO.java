package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductCollectionDTO implements Serializable {
    private List<ProductDetail> productDetail;

    public static class ProductDetail
            extends ProductDetailDTO {
        private PolicyDetailDTO policyDetail;
        private List<ProductPaymentAdviceRecordDTO> productPaymentAdviceRecordList;

        public PolicyDetailDTO getPolicyDetail() {
            return policyDetail;
        }

        public void setPolicyDetail(PolicyDetailDTO policyDetail) {
            this.policyDetail = policyDetail;
        }

        public List<ProductPaymentAdviceRecordDTO> getProductPaymentAdviceRecordList() {
            if(productPaymentAdviceRecordList == null){
                productPaymentAdviceRecordList = new ArrayList<>();
            }
            return productPaymentAdviceRecordList;
        }
    }

    public List<ProductDetail> getProductDetail() {
        if (productDetail == null){
            productDetail = new ArrayList<>();
        }
        return productDetail;
    }
}
