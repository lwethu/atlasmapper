package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class ThirdPartyPayerSupportingDocumentsDTO extends SupportingDocumentDTO implements Serializable {
    private Long proofOfAccountDocumentId;
    private Long proofOfIdentityDocumentId;
    private Long thirdPartyMandateDocumentId;
}
