package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class RequesterDetailsDTO implements Serializable {

    private long requestingEntityNumber;
    private long requestingSystemEntityNumber;
    private String requestingSystemSource;
    private String operatingSystem;
    private String analyticsReferenceNumber;

    public long getRequestingEntityNumber() {
        return requestingEntityNumber;
    }

    public void setRequestingEntityNumber(long requestingEntityNumber) {
        this.requestingEntityNumber = requestingEntityNumber;
    }

    public long getRequestingSystemEntityNumber() {
        return requestingSystemEntityNumber;
    }

    public void setRequestingSystemEntityNumber(long requestingSystemEntityNumber) {
        this.requestingSystemEntityNumber = requestingSystemEntityNumber;
    }

    public String getRequestingSystemSource() {
        return requestingSystemSource;
    }

    public void setRequestingSystemSource(String requestingSystemSource) {
        this.requestingSystemSource = requestingSystemSource;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getAnalyticsReferenceNumber() {
        return analyticsReferenceNumber;
    }

    public void setAnalyticsReferenceNumber(String analyticsReferenceNumber) {
        this.analyticsReferenceNumber = analyticsReferenceNumber;
    }
}
