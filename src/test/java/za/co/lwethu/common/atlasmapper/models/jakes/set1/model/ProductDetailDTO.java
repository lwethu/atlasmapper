package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductDetailDTO implements Serializable {
    private PolicyDetailDTO policyDetail;
    private List<ProductPaymentAdviceRecordDTO> paymentAdviceRecord;

    public PolicyDetailDTO getPolicyDetail() {
        return policyDetail;
    }

    public void setPolicyDetail(PolicyDetailDTO policyDetail) {
        this.policyDetail = policyDetail;
    }

    public List<ProductPaymentAdviceRecordDTO> getPaymentAdviceRecord() {
        if(paymentAdviceRecord == null){
            paymentAdviceRecord = new ArrayList<>();
        }
        return paymentAdviceRecord;
    }
}
