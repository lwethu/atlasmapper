package za.co.lwethu.common.atlasmapper.models;

import java.util.List;
import java.util.Set;

public class PersonB {

    private Integer id;
    private String name;
    private String age;
    private Set<String> legalRefs;
    private List<CarB> cars;

    public PersonB() {
    }

    public PersonB(Integer id, String name, String age, Set<String> legalRefs, List<CarB> cars) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.cars = cars;
        this.legalRefs = legalRefs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Set<String> getLegalRefs() {
        return legalRefs;
    }

    public void setLegalRefs(Set<String> legalRefs) {
        this.legalRefs = legalRefs;
    }

    public List<CarB> getCars() {
        return cars;
    }

    public void setCars(List<CarB> cars) {
        this.cars = cars;
    }

}
