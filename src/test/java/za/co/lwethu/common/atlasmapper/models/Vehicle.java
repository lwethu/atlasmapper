package za.co.lwethu.common.atlasmapper.models;

public class Vehicle {

    private Integer mileage;
    private byte[] image;

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

}
