package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.List;

public class UpdateBankDetailsForEntityNumberResponseDTO implements Serializable {
    private String productHouseCode;
    private List<InstructionResponseDTO> updateInstructionResponse;
}
