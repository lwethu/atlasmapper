package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;
import java.util.Calendar;

public class ProductPaymentAdviceRecordDTO implements Serializable {

    private ProductBankAccountPaymentAdviceRecordDTO productBankAccountPaymentAdviceRecord;
    private ProductOtherPaymentAdviceRecordDTO productOtherPaymentAdviceRecord;
    private String bankAccountOwnerPolicyRoleDescription;
    private Calendar effectiveFrom;
    //private BankAccountUsageEnum usage;
    private String usageDescription;
    private String uniqueReference;
    private DecisionDTO decision;

    public ProductBankAccountPaymentAdviceRecordDTO getProductBankAccountPaymentAdviceRecord() {
        return productBankAccountPaymentAdviceRecord;
    }

    public void setProductBankAccountPaymentAdviceRecord(ProductBankAccountPaymentAdviceRecordDTO productBankAccountPaymentAdviceRecord) {
        this.productBankAccountPaymentAdviceRecord = productBankAccountPaymentAdviceRecord;
    }

    public ProductOtherPaymentAdviceRecordDTO getProductOtherPaymentAdviceRecord() {
        return productOtherPaymentAdviceRecord;
    }

    public void setProductOtherPaymentAdviceRecord(ProductOtherPaymentAdviceRecordDTO productOtherPaymentAdviceRecord) {
        this.productOtherPaymentAdviceRecord = productOtherPaymentAdviceRecord;
    }

    public String getBankAccountOwnerPolicyRoleDescription() {
        return bankAccountOwnerPolicyRoleDescription;
    }

    public void setBankAccountOwnerPolicyRoleDescription(String bankAccountOwnerPolicyRoleDescription) {
        this.bankAccountOwnerPolicyRoleDescription = bankAccountOwnerPolicyRoleDescription;
    }

    public Calendar getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Calendar effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    /*public BankAccountUsageEnum getUsage() {
        return usage;
    }

    public void setUsage(BankAccountUsageEnum usage) {
        this.usage = usage;
    }*/

    public String getUsageDescription() {
        return usageDescription;
    }

    public void setUsageDescription(String usageDescription) {
        this.usageDescription = usageDescription;
    }

    public String getUniqueReference() {
        return uniqueReference;
    }

    public void setUniqueReference(String uniqueReference) {
        this.uniqueReference = uniqueReference;
    }

    public DecisionDTO getDecision() {
        return decision;
    }

    public void setDecision(DecisionDTO decision) {
        this.decision = decision;
    }
}
