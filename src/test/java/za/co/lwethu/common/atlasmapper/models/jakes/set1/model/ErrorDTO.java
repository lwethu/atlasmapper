package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class ErrorDTO implements Serializable {
    private Integer errorCode;
    private String errorMessage;
    private String jmsMessage;
    private String exception;
    private String stackTrace;
}
