//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.08.10 at 08:54:22 AM CAT 
//


package za.co.lwethu.common.atlasmapper.models.jakes.set2.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 All rules governing when banking details may be changed are owned and managed by the product house
 *                 concerned.
 *                 This system shall be completely agnostic of these rules.
 *                 This type shall contain all information related to whether the product house rules allow or disallow the
 *                 change
 *                 of banking details on the date specified.
 *             
 * 
 * <p>Java class for DecisionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DecisionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="changeAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecisionType", propOrder = {
    "changeAllowed",
    "message"
})
public class DecisionType {

    protected boolean changeAllowed;
    @XmlElement(required = true)
    protected String message;

    /**
     * Gets the value of the changeAllowed property.
     * 
     */
    public boolean isChangeAllowed() {
        return changeAllowed;
    }

    /**
     * Sets the value of the changeAllowed property.
     * 
     */
    public void setChangeAllowed(boolean value) {
        this.changeAllowed = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
