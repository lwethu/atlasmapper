package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class UpdateBankDetailsForEntityNumberRequestDTO implements Serializable {
    private String productHouseCode;
    private long representedEntityNumber;
    private String parentWorkflowProcessId;
    private UpdateBankDetailsInstructionsDTO updateBankDetailsInstructions;
}
