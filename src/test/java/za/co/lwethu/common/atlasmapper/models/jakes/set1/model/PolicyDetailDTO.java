package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class PolicyDetailDTO implements Serializable {
    private String policyNumber;
    private String policyDescription;
    private String policyEmailContactAddress;
    private boolean doesPolicyAllowThirdPartyPayer;
    private boolean doesPolicyAllowChangeOfDebitOrderDate;
    private boolean isDiscoveryCardLinkedToMember;

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyDescription() {
        return policyDescription;
    }

    public void setPolicyDescription(String policyDescription) {
        this.policyDescription = policyDescription;
    }

    public String getPolicyEmailContactAddress() {
        return policyEmailContactAddress;
    }

    public void setPolicyEmailContactAddress(String policyEmailContactAddress) {
        this.policyEmailContactAddress = policyEmailContactAddress;
    }

    public boolean isDoesPolicyAllowThirdPartyPayer() {
        return doesPolicyAllowThirdPartyPayer;
    }

    public void setDoesPolicyAllowThirdPartyPayer(boolean doesPolicyAllowThirdPartyPayer) {
        this.doesPolicyAllowThirdPartyPayer = doesPolicyAllowThirdPartyPayer;
    }

    public boolean isDoesPolicyAllowChangeOfDebitOrderDate() {
        return doesPolicyAllowChangeOfDebitOrderDate;
    }

    public void setDoesPolicyAllowChangeOfDebitOrderDate(boolean doesPolicyAllowChangeOfDebitOrderDate) {
        this.doesPolicyAllowChangeOfDebitOrderDate = doesPolicyAllowChangeOfDebitOrderDate;
    }

    public boolean isDiscoveryCardLinkedToMember() {
        return isDiscoveryCardLinkedToMember;
    }

    public void setDiscoveryCardLinkedToMember(boolean discoveryCardLinkedToMember) {
        isDiscoveryCardLinkedToMember = discoveryCardLinkedToMember;
    }
}
