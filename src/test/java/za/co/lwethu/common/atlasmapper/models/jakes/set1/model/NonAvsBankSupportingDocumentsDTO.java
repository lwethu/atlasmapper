package za.co.lwethu.common.atlasmapper.models.jakes.set1.model;

import java.io.Serializable;

public class NonAvsBankSupportingDocumentsDTO extends SupportingDocumentDTO implements Serializable {
    private Long proofOfAccountDocumentId;
    private Long proofOfIdentityDocumentId;
}
