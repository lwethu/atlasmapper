package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForLong {

    public static Long map(byte value) {
        return new Byte(value).longValue();
    }

    public static Long map(double value) {
        return new Float(value).longValue();
    }

    public static Long map(Double value) {
        return value.longValue();
    }

    public static Long map(int value) {
        return new Integer(value).longValue();
    }

    public static Long map(Integer value) {
        return value.longValue();
    }

    public static Long map(float value) {
        return new Float(value).longValue();
    }

    public static Long map(Float value) {
        return value.longValue();
    }

    public static Long map(Long value) {
        return value;
    }

    public static Long map(short value) {
        return new Short(value).longValue();
    }

    public static Long map(Short value) {
        return value.longValue();
    }

    public static Long map(String value) {
        return Long.parseLong(value);
    }

}
