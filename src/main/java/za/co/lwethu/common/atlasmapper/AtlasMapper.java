package za.co.lwethu.common.atlasmapper;

import za.co.lwethu.common.atlasmapper.annotations.MapIf;
import za.co.lwethu.common.atlasmapper.annotations.MapTo;
import za.co.lwethu.common.atlasmapper.enums.AtlasMapperParamTypeEnum;
import za.co.lwethu.common.atlasmapper.error.AtlasMapperRuntimeError;
import za.co.lwethu.common.atlasmapper.error.AtlasMapperThrowableError;
import za.co.lwethu.common.atlasmapper.model.AtlasMapperFieldContainer;
import za.co.lwethu.common.atlasmapper.model.AtlasMapperObjectContainer;
import za.co.lwethu.common.atlasmapper.services.MapIfService;
import za.co.lwethu.common.atlasmapper.services.MapToService;
import za.co.lwethu.common.atlasmapper.services.ObjectCreatorService;
import za.co.lwethu.common.atlasmapper.services.primitive.impl.PrimitiveService;

import java.beans.IntrospectionException;
import java.lang.reflect.*;
import java.util.*;

public class AtlasMapper {

    private ObjectCreatorService objectCreatorService = new ObjectCreatorService();

    @SuppressWarnings("unchecked")
    public <T> List<T> map(List inputs, Class<T> outputClass, String... params) {
        try {
            return mapListOfObjects(inputs, outputClass, new HashSet(), AtlasMapperParamTypeEnum.ALL, params);
        } catch (AtlasMapperThrowableError | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException | NoSuchFieldException | IntrospectionException e) {
            throw new AtlasMapperRuntimeError("could not map class", e);
        }
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> map(List inputs, Class<T> outputClass, AtlasMapperParamTypeEnum paramType, String... params) {
        try {
            return mapListOfObjects(inputs, outputClass, new HashSet(), paramType, params);
        } catch (AtlasMapperThrowableError | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException | NoSuchFieldException | IntrospectionException e) {
            throw new AtlasMapperRuntimeError("could not map class", e);
        }
    }

    public <T> T map(Object input, Class<T> outputClass, String... params) {
        try {
            return mapSingleObject(input, outputClass, new HashSet(), AtlasMapperParamTypeEnum.ALL, params);
        } catch (AtlasMapperThrowableError | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException | NoSuchFieldException | IntrospectionException e) {
            throw new AtlasMapperRuntimeError("could not map class", e);
        }
    }

    public <T> T map(Object input, Class<T> outputClass, AtlasMapperParamTypeEnum paramType, String... params) {
        try {
            return mapSingleObject(input, outputClass, new HashSet(), paramType, params);
        } catch (AtlasMapperThrowableError | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException | NoSuchFieldException | IntrospectionException e) {
            throw new AtlasMapperRuntimeError("could not map class", e);
        }
    }

    private <T> T mapSingleObject(Object input, Class<T> outputClass, Set objectsInProcess, AtlasMapperParamTypeEnum paramType, String... params) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException, IntrospectionException, AtlasMapperThrowableError {
        return processMapping(null, input, input != null ? input.getClass() : null, outputClass, objectsInProcess, paramType, params);
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> mapListOfObjects(List inputs, Class<T> outputClass, Set objectsInProcess, AtlasMapperParamTypeEnum paramType, String... params) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException, IntrospectionException, AtlasMapperThrowableError {
        List list = new ArrayList();
        for (Object input : inputs) {
            list.add(mapSingleObject(input, outputClass, objectsInProcess, paramType, params));
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    private <T> T processMapping(Object output, Object input, Class inputClass, Class<T> outputClass, Set objectsInProcess, AtlasMapperParamTypeEnum paramType, String... params) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException, IntrospectionException, AtlasMapperThrowableError {
        if (input == null || objectsInProcess.contains(input)) {
            return null;
        } else if (PrimitiveService.PRIMITIVE_TYPES.contains(inputClass)) {
            return (T) input;
        }
        objectsInProcess.add(input);
        if (output == null) {
            output = objectCreatorService.instantiateObject(outputClass);
        }
        AtlasMapperObjectContainer outputObject = new AtlasMapperObjectContainer(output, outputClass);
        AtlasMapperObjectContainer inputObject = new AtlasMapperObjectContainer(input, inputClass);
        if (params != null && params.length > 0) {
            for (String param : params) {
                outputObject.setValue(param, map(new AtlasMapperFieldContainer(inputObject.getValue(param), inputClass, param), new AtlasMapperFieldContainer(outputObject.getValue(param), outputClass, param), objectsInProcess, paramType, param));
            }
        } else {
            Map<Field, Field> fieldMap = getAllFields(inputClass, outputClass, paramType);
            for (Field field : fieldMap.keySet()) {
                outputObject.setValue(fieldMap.get(field), map(new AtlasMapperFieldContainer(inputObject.getValue(field), inputClass, field), new AtlasMapperFieldContainer(outputObject.getValue(fieldMap.get(field)), outputClass, fieldMap.get(field)), objectsInProcess, paramType, field.getName()));
            }
        }
        objectsInProcess.remove(inputObject.getValue());
        return (T) outputObject.getValue();
    }

    @SuppressWarnings("unchecked")
    private Object map(AtlasMapperFieldContainer inputField, AtlasMapperFieldContainer outputField, Set objectsInProcess, AtlasMapperParamTypeEnum paramType, String param) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException, NoSuchFieldException, IntrospectionException, AtlasMapperThrowableError {
        if (param.contains(".")) {
            List<String> params = Arrays.asList(param.split("\\.", 2));
            if (isMultitude(outputField.getField().getType())) {
                outputField.setValue(mapMultitude(inputField, outputField, objectsInProcess, paramType, params.get(1)));
            } else {
                outputField.setValue(processMapping(outputField.getValue(), inputField.getValue(), inputField.getField().getType(), outputField.getField().getType(), objectsInProcess, paramType, params.get(1)));
            }
        } else {
            MapIf mapIf = inputField.getField().getAnnotation(MapIf.class);
            MapTo mapTo = inputField.getField().getAnnotation(MapTo.class);
            Boolean shouldMap = true;
            if (mapIf != null) {
                MapIfService mapIfService = mapIf.value().newInstance();
                Method method = mapTo.value().getMethod("map", Object.class);
                shouldMap = (Boolean) method.invoke(mapIfService, inputField.getValue());
            }

            if (!shouldMap) {
                outputField.setValue(null);
            } else if (mapTo != null) {
                MapToService mapToService = mapTo.value().newInstance();
                Method method = mapTo.value().getMethod("map", Object.class);
                Object value = method.invoke(mapToService, inputField.getValue());
                outputField.setValue(value);
            } else if (PrimitiveService.PRIMITIVE_TYPES.contains(inputField.getField().getType())) {
                outputField.setValue(inputField.getValue());
            } else if (inputField.getField().getType().isEnum()) {
                outputField.setValue(inputField.getValue());
            } else if (isMultitude(inputField.getField().getType())) {
                outputField.setValue(mapMultitude(inputField, outputField, objectsInProcess, paramType));
            } else {
                outputField.setValue(mapSingleObject(inputField.getValue(), outputField.getField().getType(), objectsInProcess, paramType));
            }
        }
        return outputField.getValue();
    }

    private Object mapMultitude(AtlasMapperFieldContainer input, AtlasMapperFieldContainer output, Set objectsInProcess, AtlasMapperParamTypeEnum paramType, String... params) throws IllegalAccessException, IntrospectionException, InstantiationException, AtlasMapperThrowableError, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        if (input.getValue() == null) {
            return null;
        }
        return input.getField().getType().isArray() ? mapArray(input, output, objectsInProcess, paramType, params) : mapCollection(input, output, objectsInProcess, paramType, params);
    }

    private Collection mapCollection(AtlasMapperFieldContainer input, AtlasMapperFieldContainer output, Set objectsInProcess, AtlasMapperParamTypeEnum paramType, String... params) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException, NoSuchFieldException, IntrospectionException, AtlasMapperThrowableError {
        Type[] genericClasses = ((ParameterizedType) output.getField().getGenericType()).getActualTypeArguments();
        Collection collection = (Collection) objectCreatorService.instantiateObject(output.getField().getType());
        if (input.getValue() == null) {
            return null;
        }
        for (Object inputListItem : ((Collection) input.getValue())) {
            Object outputValue = output.getValue() != null && output.getValue() instanceof List ? ((List) output.getValue()).get(((List) input.getValue()).indexOf(inputListItem)) : null;
            collection.add(processMapping(outputValue, inputListItem, inputListItem.getClass(), (Class<?>) genericClasses[0], objectsInProcess, paramType, params));
        }
        return collection;
    }

    private Object mapArray(AtlasMapperFieldContainer input, AtlasMapperFieldContainer output, Set objectsInProcess, AtlasMapperParamTypeEnum paramType, String... params) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, IntrospectionException, InstantiationException, AtlasMapperThrowableError, NoSuchFieldException {
        Integer length = Array.getLength(input.getValue());
        Class type = byte.class;
        Object objects = Array.newInstance(type, length);
        for (Integer i = 0; i < length; i++) {
            Byte inputValue = (Byte) Array.get(input.getValue(), i);
            Object outputValue = output.getValue() != null && output.getValue() instanceof List ? ((List) output.getValue()).get(((List) input.getValue()).indexOf(inputValue)) : null;
            Object value = processMapping(outputValue, inputValue.byteValue(), type, type, objectsInProcess, paramType, params);
            Array.set(objects, i, value);
        }
        return objects;
    }

    private Map<Field, Field> getAllFields(Class inputClass, Class outputClass, AtlasMapperParamTypeEnum paramType) {
        Map<Field, Field> fieldMap = new HashMap<>();
        Map<String, Field> outputFielddMap = new HashMap<>();
        List<String> fieldNames = new ArrayList<>();
        for (Field field : getAllFields(outputClass)) {
            if (!field.getName().substring(0, 1).equals("$") && isFieldCompatableWithType(field, paramType)) {
                fieldNames.add(field.getName());
                outputFielddMap.put(field.getName(), field);
            }
        }
        for (Field field : getAllFields(inputClass)) {
            if (fieldNames.contains(field.getName())) {
                fieldMap.put(field, outputFielddMap.get(field.getName()));
            }
        }
        return fieldMap;
    }

    private Boolean isFieldCompatableWithType(Field field, AtlasMapperParamTypeEnum paramType) {
        Boolean result = true;
        if (paramType == AtlasMapperParamTypeEnum.ALL) {
            result = true;
        } else if (paramType == AtlasMapperParamTypeEnum.BASIC) {
            result = !isMultitude(field.getType());
        }
        return result;
    }

    private List<Field> getAllFields(Class type) {
        List<Field> fields = new ArrayList<>();
        for (Field field : type.getDeclaredFields()) {
            fields.add(field);
        }
        if (type.getSuperclass() != null && type.getSuperclass() != Object.class) {
            List<Field> newFields = getAllFields(type.getSuperclass());
            fields.addAll(newFields);
        }
        return fields;
    }

    private Boolean isMultitude(Class type) {
        return Collection.class.isAssignableFrom(type) || type.isArray();
    }

}
