package za.co.lwethu.common.atlasmapper.services;

import za.co.lwethu.common.atlasmapper.error.AtlasMapperThrowableError;

import java.util.*;

public class ObjectCreatorService {

    private static Map<Class<? extends Collection>, Class> collectionMap = new HashMap<>();

    static {
        collectionMap.put(List.class, ArrayList.class);
        collectionMap.put(Set.class, HashSet.class);
    }

    public Object instantiateObject(Class objectClass) throws IllegalAccessException, InstantiationException, AtlasMapperThrowableError {
        Object object;
        if (Collection.class.isAssignableFrom(objectClass)) {
            object = collectionMap.get(objectClass).newInstance();
        } else if (objectClass.isEnum()) {
            object = null;
        } else {
            object = objectClass.newInstance();
        }
        return object;
    }

}
