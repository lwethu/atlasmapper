package za.co.lwethu.common.atlasmapper.model;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class AtlasMapperFieldContainer {

    private Field field;
    private Object object;
    private Class root;

    public AtlasMapperFieldContainer(Object object, Class root, String name) throws NoSuchFieldException, IntrospectionException {
        this.object = object;
        this.root = root;
        this.field = getField(root, name);
    }

    public AtlasMapperFieldContainer(Object object, Class root, Field field) throws NoSuchFieldException, IntrospectionException {
        this.object = object;
        this.root = root;
        this.field = field;
    }

    public Object getValue() throws InvocationTargetException, IllegalAccessException {
        return this.object;
    }

    public void setValue(Object value) throws InvocationTargetException, IllegalAccessException {
        this.object = value;
    }

    public Field getField() {
        return field;
    }

    private Field getField(Class aClass, String param) throws NoSuchFieldException {
        String result = param.contains(".") ? param.split("\\.", 2)[0] : param;
        return aClass.getDeclaredField(result);
    }

}
