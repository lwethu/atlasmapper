package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForString {

    public static String map(byte value) {
        return String.valueOf(value);
    }

    public static String map(char value) {
        return String.valueOf(value);
    }

    public static String map(double value) {
        return String.valueOf(value);
    }

    public static String map(float value) {
        return String.valueOf(value);
    }

    public static String map(long value) {
        return String.valueOf(value);
    }

    public static String map(Boolean value) {
        return String.valueOf(value);
    }

    public static String map(Byte value) {
        return String.valueOf(value);
    }

    public static String map(Character value) {
        return String.valueOf(value);
    }

    public static String map(Double value) {
        return String.valueOf(value);
    }

    public static String map(Float value) {
        return String.valueOf(value);
    }

    public static String map(Integer value) {
        return String.valueOf(value);
    }

    public static String map(Long value) {
        return String.valueOf(value);
    }

    public static String map(Short value) {
        return String.valueOf(value);
    }

}
