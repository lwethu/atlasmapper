package za.co.lwethu.common.atlasmapper.model;

import za.co.lwethu.common.atlasmapper.services.primitive.impl.PrimitiveService;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class AtlasMapperObjectContainer {

    private PrimitiveService primitiveService = new PrimitiveService();
    private Object object;
    private Class clazz;

    public AtlasMapperObjectContainer(Object object, Class clazz) {
        this.object = object;
        this.clazz = clazz;
    }

    public Object getValue() {
        return this.object;
    }

    public Object getValue(Field field) throws InvocationTargetException, IllegalAccessException, IntrospectionException {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), field.getDeclaringClass());
        return propertyDescriptor.getReadMethod().invoke(this.object);
    }

    public Object getValue(String param) throws InvocationTargetException, IllegalAccessException, IntrospectionException, NoSuchFieldException {
        Field field = getField(this.clazz, getParam(param));
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getParam(param), field.getDeclaringClass());
        return propertyDescriptor.getReadMethod().invoke(this.object);
    }

    public void setValue(Field field, Object value) throws InvocationTargetException, IllegalAccessException, IntrospectionException {
        if (areValuesDifferentPrimitives(value, field.getType())) {
            value = primitiveService.map(value, field.getType());
        }
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), field.getDeclaringClass());
        propertyDescriptor.getWriteMethod().invoke(this.object, value);
    }

    public void setValue(String param, Object value) throws InvocationTargetException, IllegalAccessException, IntrospectionException, NoSuchFieldException {
        Field field = getField(this.clazz, getParam(param));
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getParam(param), field.getDeclaringClass());
        propertyDescriptor.getWriteMethod().invoke(this.object, value);
    }

    private Field getField(Class aClass, String param) throws NoSuchFieldException {
        String result = getParam(param);
        return aClass.getDeclaredField(result);
    }

    private String getParam(String param) {
        return param.contains(".") ? param.split("\\.", 2)[0] : param;
    }

    private Boolean areValuesDifferentPrimitives(Object value, Class aClass) {
        return value != null
                && value.getClass() != aClass
                && primitiveService.PRIMITIVE_TYPES.contains(value.getClass())
                && primitiveService.PRIMITIVE_TYPES.contains(aClass);
    }

}
