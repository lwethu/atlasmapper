package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForDoublePrimitive {

    public static double map(byte value) {
        return new Byte(value).doubleValue();
    }

    public static double map(double value) {
        return value;
    }

    public static double map(Double value) {
        return value;
    }

    public static double map(int value) {
        return new Integer(value).doubleValue();
    }

    public static double map(Integer value) {
        return value.doubleValue();
    }

    public static double map(float value) {
        return value;
    }

    public static double map(Float value) {
        return value;
    }

    public static double map(long value) {
        return new Long(value).doubleValue();
    }
    
    public static double map(Long value) {
        return value.doubleValue();
    }

    public static double map(short value) {
        return new Short(value).doubleValue();
    }

    public static double map(Short value) {
        return value.doubleValue();
    }

    public static double map(String value) {
        return Double.parseDouble(value);
    }

}
