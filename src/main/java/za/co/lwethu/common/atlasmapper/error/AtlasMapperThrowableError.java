package za.co.lwethu.common.atlasmapper.error;

public class AtlasMapperThrowableError extends Throwable {

    public AtlasMapperThrowableError(String message) {
        super(message);
    }
}
