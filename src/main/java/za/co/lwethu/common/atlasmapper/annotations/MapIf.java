package za.co.lwethu.common.atlasmapper.annotations;

import za.co.lwethu.common.atlasmapper.services.MapIfService;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MapIf {

    Class<? extends MapIfService> value();

}
