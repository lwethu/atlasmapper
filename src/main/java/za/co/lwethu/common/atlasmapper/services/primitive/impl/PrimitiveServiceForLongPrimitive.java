package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForLongPrimitive {

    public static long map(byte value) {
        return new Byte(value).longValue();
    }

    public static long map(double value) {
        return new Float(value).longValue();
    }

    public static long map(Double value) {
        return value.longValue();
    }

    public static long map(int value) {
        return new Integer(value).longValue();
    }

    public static long map(Integer value) {
        return value.longValue();
    }

    public static long map(float value) {
        return new Float(value).longValue();
    }

    public static long map(Float value) {
        return value.longValue();
    }

    public static long map(Long value) {
        return value;
    }

    public static long map(short value) {
        return value;
    }

    public static long map(Short value) {
        return value;
    }

    public static long map(String value) {
        return Long.parseLong(value);
    }

}
