package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForFloatPrimitive {

    public static float map(byte value) {
        return new Byte(value).floatValue();
    }

    public static float map(double value) {
        return new Double(value).floatValue();
    }

    public static float map(Double value) {
        return value.floatValue();
    }

    public static float map(int value) {
        return new Integer(value).floatValue();
    }

    public static float map(Integer value) {
        return value.floatValue();
    }

    public static float map(float value) {
        return value;
    }

    public static float map(Float value) {
        return value;
    }

    public static float map(long value) {
        return new Long(value).floatValue();
    }

    public static float map(Long value) {
        return value.floatValue();
    }

    public static float map(short value) {
        return new Short(value).floatValue();
    }

    public static float map(Short value) {
        return value.floatValue();
    }

    public static float map(String value) {
        return Float.parseFloat(value);
    }

}
