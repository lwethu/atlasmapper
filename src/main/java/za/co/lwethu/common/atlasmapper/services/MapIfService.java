package za.co.lwethu.common.atlasmapper.services;

public abstract class MapIfService {

    public abstract Boolean map(Object value);

}
