package za.co.lwethu.common.atlasmapper.services.primitive.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class PrimitiveService {

    public static List PRIMITIVE_TYPES = Arrays.asList(
            byte.class, char.class, double.class, float.class, int.class, long.class, short.class, void.class,
            Boolean.class, Byte.class, Character.class, Date.class, Double.class, Float.class, Integer.class, Long.class, Short.class, String.class, Void.class);
    private static Map<Class, Class> serviceMap = new HashMap<>();

    static {
        serviceMap.put(String.class, PrimitiveServiceForString.class);
        serviceMap.put(int.class, PrimitiveServiceForIntPrimitive.class);
        serviceMap.put(Integer.class, PrimitiveServiceForInteger.class);
        serviceMap.put(float.class, PrimitiveServiceForFloatPrimitive.class);
        serviceMap.put(Float.class, PrimitiveServiceForFloat.class);
        serviceMap.put(long.class, PrimitiveServiceForLongPrimitive.class);
        serviceMap.put(Long.class, PrimitiveServiceForLong.class);
        serviceMap.put(double.class, PrimitiveServiceForDoublePrimitive.class);
        serviceMap.put(Double.class, PrimitiveServiceForDouble.class);
    }

    public Object map(Object object, Class aClass) {
        Object result;
        try {
            Class service = serviceMap.get(aClass);
            Method method = service.getMethod("map", object.getClass());
            result = method.invoke(this, object);
        } catch (Exception e) {
            throw new ArithmeticException("Cannot map " + object.getClass() + " to " + aClass);
        }
        return result;
    }

    private static class ClassGroup {

        private Class classA;
        private Class classB;

        public ClassGroup(Class classA, Class classB) {
            this.classA = classA;
            this.classB = classB;
        }

        public Class getClassA() {
            return classA;
        }

        public void setClassA(Class classA) {
            this.classA = classA;
        }

        public Class getClassB() {
            return classB;
        }

        public void setClassB(Class classB) {
            this.classB = classB;
        }

        @Override
        public boolean equals(Object o) {
            return this.getClassA() == ((ClassGroup) o).getClassA()
                    && this.classB == ((ClassGroup) o).getClassB();
        }

        @Override
        public int hashCode() {
            return classA.hashCode() + classB.hashCode();
        }
    }

}