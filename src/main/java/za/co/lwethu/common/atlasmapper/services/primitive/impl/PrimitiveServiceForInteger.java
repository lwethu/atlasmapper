package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForInteger {

    public static Integer map(byte value) {
        return new Byte(value).intValue();
    }

    public static Integer map(double value) {
        return new Float(value).intValue();
    }

    public static Integer map(Double value) {
        return value.intValue();
    }

    public static Integer map(Integer value) {
        return value;
    }

    public static Integer map(float value) {
        return new Float(value).intValue();
    }

    public static Integer map(Float value) {
        return value.intValue();
    }

    public static Integer map(short value) {
        return new Short(value).intValue();
    }

    public static Integer map(Short value) {
        return value.intValue();
    }

    public static Integer map(long value) {
        return new Long(value).intValue();
    }

    public static Integer map(Long value) {
        return value.intValue();
    }

    public static Integer map(String value) {
        return Integer.parseInt(value);
    }

}
