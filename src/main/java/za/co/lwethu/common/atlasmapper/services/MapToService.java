package za.co.lwethu.common.atlasmapper.services;

public abstract class MapToService {

    public abstract Object map(Object value);

}
