package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForDouble {

    public static Double map(byte value) {
        return new Byte(value).doubleValue();
    }

    public static Double map(double value) {
        return value;
    }

    public static Double map(Double value) {
        return value;
    }

    public static Double map(int value) {
        return new Integer(value).doubleValue();
    }

    public static Double map(Integer value) {
        return value.doubleValue();
    }

    public static Double map(float value) {
        return new Float(value).doubleValue();
    }

    public static Double map(Float value) {
        return value.doubleValue();
    }

    public static Double map(long value) {
        return new Long(value).doubleValue();
    }
    
    public static Double map(Long value) {
        return value.doubleValue();
    }

    public static Double map(short value) {
        return new Short(value).doubleValue();
    }

    public static Double map(Short value) {
        return value.doubleValue();
    }

    public static Double map(String value) {
        return Double.parseDouble(value);
    }

}
