package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForFloat {

    public static Float map(byte value) {
        return new Byte(value).floatValue();
    }

    public static Float map(double value) {
        return new Double(value).floatValue();
    }

    public static Float map(Double value) {
        return value.floatValue();
    }

    public static Float map(int value) {
        return new Integer(value).floatValue();
    }

    public static Float map(Integer value) {
        return value.floatValue();
    }

    public static Float map(float value) {
        return value;
    }

    public static Float map(Float value) {
        return value;
    }

    public static Float map(long value) {
        return new Long(value).floatValue();
    }

    public static Float map(Long value) {
        return value.floatValue();
    }

    public static Float map(short value) {
        return new Short(value).floatValue();
    }

    public static Float map(Short value) {
        return value.floatValue();
    }

    public static Float map(String value) {
        return Float.parseFloat(value);
    }

}
