package za.co.lwethu.common.atlasmapper.error;

public class AtlasMapperRuntimeError extends RuntimeException {

    public AtlasMapperRuntimeError(String message) {
        super(message);
    }

    public AtlasMapperRuntimeError(String message, Throwable cause) {
        super(message, cause);
    }

}
