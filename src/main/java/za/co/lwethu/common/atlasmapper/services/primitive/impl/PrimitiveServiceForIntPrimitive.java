package za.co.lwethu.common.atlasmapper.services.primitive.impl;

public class PrimitiveServiceForIntPrimitive {

    public static int map(byte value) {
        return new Byte(value).intValue();
    }

    public static int map(double value) {
        return new Float(value).intValue();
    }

    public static int map(Double value) {
        return value.intValue();
    }

    public static int map(Integer value) {
        return value;
    }

    public static int map(float value) {
        return new Float(value).intValue();
    }

    public static int map(Float value) {
        return value.intValue();
    }

    public static int map(short value) {
        return value;
    }

    public static int map(Short value) {
        return value;
    }

    public static int map(long value) {
        return new Long(value).intValue();
    }

    public static int map(Long value) {
        return value.intValue();
    }

    public static int map(String value) {
        return Integer.parseInt(value);
    }

}
